#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Tests to make sure that the correct contexts are acquired, automatically.'''

# IMPORT STANDARD LIBRARIES
from __future__ import print_function
import unittest

# IMPORT THIRD-PARTY LIBRARIES
from code_contexts import manager
import code_contexts


class CommonMixin(object):  # pylint: disable=R0903

    '''A basic interface with simple objects that are used in other tests.'''

    example_file = \
'''\
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# IMPORT STANDARD LIBRARIES
import something
import another_module

def some_function():
    """Some docstring of the function."""
    pass

'''
    top_row = 0
    some_list_comp_assignment = 'somevar = [    ]'
    some_list_comp_assignment_incomplete = 'somevar = [    '
    some_list_comp_name = '[    ]'
    some_list_comp_name_incomplete = '[   '

    some_generator_comp_assignment = 'somevar = (    )'
    some_generator_comp_assignment_incomplete = 'somevar = (    '
    some_generator_comp_name = '(    )'
    some_generator_comp_name_incomplete = '(   '
    with_statment = '''\
with asdasdsdf as f:
    something
'''

    def __init__(self, *args, **kwargs):
        '''Initialize the mixin class.

        Args:
            *args: Any arbitrary args to send to the class
            *kwargs: Any arbitrary kwargs to send to the class

        '''
        super(CommonMixin, self).__init__(*args, **kwargs)
        self.shebang_cls = code_contexts.ShebangSetting
        self.docstring_cls = code_contexts.ModuleDocstringSetting
        self.import_cls = code_contexts.ImportSetting
    # end __init__
# end CommonMixin


class ManagerTestCase(CommonMixin, unittest.TestCase):

    '''A basic class that ensures that contexts pass and fail correctly.'''

    def test_row_eoferror_true(self):
        '''A passing test for when there are more lines than the given row.'''
        assertion = True
        try:
            _ = manager.get_code_context(
                row=-3, column=0, code=self.example_file)
        except manager.MaximumValueExceededError:
            assertion = False

        self.assertTrue(assertion)
    # end test_row_eoferror_true

    def test_row_eoferror_false(self):
        '''A passing test for when there are less lines than the given row.'''
        assertion = False
        try:
            _ = manager.get_code_context(
                row=-3, column=0, code=self.example_file)
        except manager.MaximumValueExceededError:
            assertion = True

        self.assertFalse(assertion)
    # end test_row_eoferror_false

    def test_col_maximum_false(self):
        '''A passing test where the column of a line is an acceptable value.'''
        assertion = True
        try:
            _ = manager.get_code_context(
                row=self.top_row,
                column=len(self.example_file[self.top_row]) - 1,
                code=self.example_file)
        except manager.MaximumValueExceededError:
            assertion = False

        self.assertTrue(assertion)
    # end test_col_maximum_false

    def test_col_maximum_true(self):
        '''A passing test where the column of a line is a bad value.'''
        assertion = False
        try:
            _ = manager.get_code_context(
                row=self.top_row,
                column=len(self.example_file[self.top_row]) + 1,
                code=self.example_file)
        except manager.MaximumValueExceededError:
            assertion = False

        self.assertFalse(assertion)
    # end test_col_maximum_true
# end ManagerTestCase


class SettingGetTestCase(CommonMixin, unittest.TestCase):

    '''A basic class that tries to automatically get every known setting.'''

    def test_shebang_true(self):
        '''Test that the shebang context works with some given code.'''
        context = manager.get_code_context(
            row=0, column=0, code=self.example_file)
        self.assertEqual(context.type(), self.shebang_cls.type())
    # end test_shebang_true

    def test_shebang_true_no_code(self):
        '''Test the shebang context, which should return True with no code.'''
        context = manager.get_code_context(
            row=0, column=0, code='')
        self.assertEqual(context.type(), self.shebang_cls.type())
    # end test_shebang_true_no_code

    def test_shebang_false(self):
        '''Test that the shebang context fails when it's expected to fail.'''
        self.assertFalse(self.shebang_cls.is_valid(row=1, column=0, code=''))
    # end test_shebang_false

    def test_module_docstring_true(self):
        '''Given some WIP file, test that the docstring could be found.'''
        context = manager.get_code_context(
            row=1, column=0, code=self.example_file)
        self.assertEqual(context.type(), self.docstring_cls.type())
    # end test_module_docstring_true

    def test_module_docstring_true(self):
        '''Given some WIP file, test that the docstring could be found.'''
        context = manager.get_code_context(
            row=1, column=0, code=self.example_file)
        self.assertEqual(context.type(), self.docstring_cls.type())
    # end test_module_docstring_true

    def test_module_docstring_false(self):
        '''Given some WIP file, test that the docstring should fail.'''
        try:
            context = manager.get_code_context(
                row=len(self.example_file.split('\n')) - 1,
                column=0,
                code=self.example_file)
            self.assertNotEqual(context.type(), self.docstring_cls.type())
        except NotImplementedError:
            # TODO : Need to make this work for strings, as well as vim buffers
            pass
    # end test_module_docstring_false

    def test_import_true(self):
        '''Given some WIP file, test if the current line is an import line.'''
        context = manager.get_code_context(
            row=6, column=0, code=self.example_file)
        self.assertEqual(context.type(), self.import_cls.type())
    # end test_import_true

    def test_import_false_above(self):
        '''Given some file, the line is too high up to be an import line.'''
        context = manager.get_code_context(
            row=4, column=0, code=self.example_file)
        self.assertNotEqual(context.type(), self.import_cls.type())
    # end test_import_false_above

    def teest_import_false_below(self):
        '''Given some file, the line is too low to be an import line.'''
        context = manager.get_code_context(
            row=8, column=0, code=self.example_file)
        self.assertNotEqual(context.type(), self.import_cls.type())
    # end teest_import_false_below

    # TODO : There needs to be some reasonably intelligent way to specify the
    #        types of contexts to search for. For example, there are positional
    #        contexts, like shebang, and inline, like comment
    #
# end SettingGetTestCase


# TODO : For the raise/except snippet variants, make raise call the except
#        snippet and then just append ()s to the end
# TODO : DoctringSetting - need to fix vimmock so that it works with Buffer
# TODO : Make a snippet called open(\w), where the captured \w becomes the open
#        mode
# TODO : The line-related contexts, such as ComphrensionSetting, need tests
# TODO : The line-related contexts, such as ForLoogSetting, need tests
# TODO : The line-related contexts, such as GlobalSetting, need tests
# TODO : The line-related contexts, such as WithSetting, need tests
#           - Still need to make it work for within the block
# TODO : The line-related contexts, such as IfSetting, need tests
# TODO : The line-related contexts, such as CommentSetting, need tests
# TODO : try/except context
class IsValidTestCase(CommonMixin, unittest.TestCase):

    '''A simple class that tests each setting for explicit inputs.

    If bugs appear in the future or weird outlier code that breaks a
    setting, add the fix here.

    '''

    def test_comphrension_braces_variable_assignment_true(self):
        '''Given some list comprehension, detect if the cursor is inside it.'''
        column = self.some_list_comp_assignment.index('[')
        self.assertTrue(code_contexts.ComphrensionSetting.is_valid(
            row=0, column=column, code=self.some_list_comp_assignment))
    # end test_comphrension_braces_variable_assignment_true

    def test_comphrension_braces_variable_assignment_all_values_true(self):
        '''Given some comprehension, see if the cursor is anywhere inside.'''
        start = self.some_list_comp_assignment.index('[')
        end = self.some_list_comp_assignment.index(']')

        output_values = []
        for column in xrange(start, end):
            output_values.append(code_contexts.ComphrensionSetting.is_valid(
                row=0, column=column, code=self.some_list_comp_assignment))

        # TODO : Ew. Make this work with ExpectingTestCase
        self.assertTrue(all(output_values) and output_values[0])
    # end test_comphrension_braces_variable_assignment_all_values_true

    def test_comphrension_braces_var_assignment_incomplete_all_true(self):
        '''Given some comprehension, see if the cursor is anywhere inside.'''
        start = self.some_list_comp_assignment_incomplete.index('[')
        end = len(self.some_list_comp_assignment_incomplete)

        output_values = []
        for column in xrange(start, end):
            output_values.append(code_contexts.ComphrensionSetting.is_valid(
                row=0, column=column,
                code=self.some_list_comp_assignment_incomplete))

        # TODO : Ew. Make this work with ExpectingTestCase
        self.assertTrue(all(output_values) and output_values[0])
    # end test_comphrension_braces_var_assignment_incomplete_all_true

    def test_comphrension_braces_variable_assignment_false(self):
        '''Given some list comprehension, detect if the cursor is inside it.'''
        column = self.some_list_comp_assignment.index(']')
        self.assertFalse(code_contexts.ComphrensionSetting.is_valid(
            row=0, column=column + 1,
            code=self.some_list_comp_name))
    # end test_comphrension_braces_variable_assignment_false

    def test_comphrension_braces_name_true(self):
        '''Given some list comprehension, detect if the cursor is inside it.'''
        column = self.some_list_comp_name.index('[')
        self.assertTrue(code_contexts.ComphrensionSetting.is_valid(
            row=0, column=column, code=self.some_list_comp_name))
    # end test_comphrension_braces_name_true

    def test_comphrension_braces_variable_name_all_values_true(self):
        '''Given some comprehension, see if the cursor is anywhere inside.'''
        start = self.some_list_comp_name.index('[')
        end = self.some_list_comp_name.index(']')

        output_values = []
        for column in xrange(start, end):
            output_values.append(code_contexts.ComphrensionSetting.is_valid(
                row=0, column=column, code=self.some_list_comp_name))

        # TODO : Ew. Make this work with ExpectingTestCase
        self.assertTrue(all(output_values) and output_values[0])
    # end test_comphrension_braces_variable_name_all_values_true

    def test_comphrension_braces_var_name_incomplete_all_true(self):
        '''Given some comprehension, see if the cursor is anywhere inside.'''
        start = self.some_list_comp_name_incomplete.index('[')
        end = len(self.some_list_comp_name_incomplete)

        output_values = []
        for column in xrange(start, end):
            output_values.append(code_contexts.ComphrensionSetting.is_valid(
                row=0, column=column,
                code=self.some_list_comp_name_incomplete))

        # TODO : Ew. Make this work with ExpectingTestCase
        self.assertTrue(all(output_values) and output_values[0])
    # end test_comphrension_braces_var_name_incomplete_all_true

    def test_comphrension_braces_name_false(self):
        '''Given some list comprehension, detect if the cursor is inside it.'''
        column = self.some_list_comp_name.index(']')
        self.assertFalse(code_contexts.ComphrensionSetting.is_valid(
            row=0, column=column + 1, code=self.some_list_comp_name))
    # end test_comphrension_braces_name_false

    def test_with_statement_inline_true(self):
        '''Given a with block, test that only the top-with statement passes.'''
        self.assertTrue(code_contexts.WithSetting.is_valid(row=0, column=0,
                                                     code=self.with_statment))
    # end test_with_statement_inline_true

    def test_with_statement_inline_false(self):
        '''Given a with block, test that only the top-with statement fails.'''
        self.assertFalse(code_contexts.WithSetting.is_valid(row=1, column=0,
                                                      code=self.with_statment))
    # end test_with_statement_inline_false

    # TODO : Make sure to add tests for () generators
# end IsValidTestCase


# TODO : Really need to add tests for is_valid_snip, too


if __name__ == '__main__':
    print(__doc__)
