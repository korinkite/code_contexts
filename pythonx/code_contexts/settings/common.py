#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Provide some common abstract/classes for all Setting objects.'''

# IMPORT STANDARD LIBRARIES
import abc


class SettingAbstractBaseClass(object):

    '''A simple interface that determines the implementation a code context.'''

    _type = ''

    def __init__(self):
        '''Initialize the basic metaclass.'''
        super(SettingAbstractBaseClass, self).__init__()
    # end __init__

    @classmethod
    @abc.abstractmethod
    def is_valid(cls, row, column=0, code=''):  # pylint: disable=C0202
        '''Determine if the row/column position within the code is valid.

        Args:
            row (int): The (aka line number) of the code
            column (:obj:`int`, optional): The column number of the code
            code (:obj:`str`, optional): The code to check

        Returns:
            bool: Checks if the context of the written code is expected

        '''
        pass
    # end is_valid

    @classmethod
    @abc.abstractmethod
    def is_valid_snip(cls, snip):
        '''A wrapper that will determine if the given snippet is valid.

        Args:
            snip (<SnippetUtilForAction>): The snippet to check for validity

        '''
        pass
    # end is_valid_snip

    @classmethod
    @abc.abstractmethod
    def get_default(cls):
        '''The default display of that context class.'''
        pass
    # end get_default

    @classmethod
    def type(cls):
        '''str:The type of the class, which must come from cls._type.'''
        return cls._type
    # end type
# end SettingAbstractBaseClass


if __name__ == '__main__':
    print(__doc__)
