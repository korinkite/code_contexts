#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Mixin classes that are used to construct and manage Setting objects.'''

# IMPORT THIRD-PARTY LIBRARIES
import vim

# IMPORT LOCAL LIBRARIES
from ..core import check


class CommonSettingMixin(object):

    '''A basic object that aims to make other setting classes less complex.'''

    def __init__(self):
        '''Initialize the object.'''
        super(CommonSettingMixin, self).__init__()
    # end __init__

    @classmethod
    def is_valid_snip(cls, snip):
        '''A wrapper that will determine if the given snippet is valid.

        Args:
            snip (<SnippetUtilForAction>): The snippet to check for validity

        Returns:
            bool: Checks if the current position is at the shebang line

        '''
        return cls.is_valid(row=snip.line, column=snip.column, code=snip.buffer)

    @classmethod
    def resolve_code(cls, code):
        '''Make code iterable or substitute it with vim's buffer, if it can't.

        Args:
            code (list[str] or str): The code to make iterable

        Returns:
            list[str] or <vim.current.buffer>: The iterable code

        '''
        if isinstance(code, vim.Buffer):
            return list(code)

        if code:  # could be [] or '', for example
            if not check.is_itertype(code):
                code = code.split('\n')
            return code

        # If no code was provided, as a fallback, provide the whole buffer
        return vim.current.buffer


class StartingLineCommonMixin(object):

    '''A simple class that defines the very top lines of a python file.'''

    allowed_row_numbers = tuple()

    def __init__(self):
        '''Initialize the object.'''
        super(StartingLineCommonMixin, self).__init__()
    # end __init__

    @classmethod
    def is_valid(cls, row, column=0, code=''):
        '''Determine if the row/column position within the code is valid.

        Args:
            row (int): The (aka line number) of the code
            column (:obj:`int`, optional): The column number of the code
            code (:obj:`str): The code to check

        Returns:
            bool: Checks if the current position is at the shebang line

        '''
        return row in cls.allowed_row_numbers


if __name__ == '__main__':
    print(__doc__)

