#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''A module that helps write Setting objects defined at the top of a file.'''

# IMPORT LOCAL LIBRARIES
from .common import SettingAbstractBaseClass
from .mixin import CommonSettingMixin


class CommonTopSetting(CommonSettingMixin, SettingAbstractBaseClass):

    '''A basic class for common methods amongst all top classes.

    This class is not registered in the main REGISTRY and is just meant to
    save from code duplication.

    Most settings that define the top of a file have the same/similar structure
    and so this class aims to genericize them all.

    '''

    ignore_prefixes = ('',)

    def __init__(self):
        '''Initialize the class instance.'''
        super(CommonTopSetting, self).__init__()
    # end __init__

    @classmethod
    def is_valid(cls, row, column=0, code=''):
        '''Determine if the row/column position within the code is valid.

        Args:
            row (int): The (aka line number) of the code
            column (:obj:`int`, optional): The column number of the code
            code (:obj:`str`, optional): The code to check

        Returns:
            bool: If the current position is at the shebang line

        '''
        code = cls.resolve_code(code=code)

        for line_number in range(row):
            line = code[line_number].strip()
            if not cls.line_check(line=line):
                return False
        return True
    # end is_valid

    @classmethod
    def line_check(cls, line):
        '''Make sure that a given line is valid.

        Args:
            line (str): The line to check

        Returns:
            bool: Whether or not the line is valid

        '''
        return not line.strip() or (
            line.strip() and line.startswith(cls.ignore_prefixes))
    # end line_check
# end CommonTopSetting


if __name__ == '__main__':
    print(__doc__)
