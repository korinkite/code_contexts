#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''A series of context functions that can be used for Vim-python snippets.'''

# IMPORT THIRD-PARTY LIBRARIES
import vim

# IMPORT LOCAL LIBRARIES
from . import visitors_and_helpers as visitors


def _get_indent(text):
    '''str: Get the full indent text.'''
    return text[:len(text) - len(text.lstrip())]


def is_valid_import_line():
    '''bool: Check if the user is on a line that would be "appropriate" for importing.

    And by "appropriate", that means:

    1. At the top of the file after the module-level docstring if it exists
    2. At the start of a function.

    Returns:
        bool: If the user's current line is valid.

    '''
    def _find_indent(lines, row):
        try:
            return _get_indent(lines[row + 1])
        except IndexError:
            pass

        return _get_indent(lines[row - 1]) + '    '

    lines = vim.current.buffer[:]
    (row, _) = vim.current.window.cursor
    current_row = row - 1

    # Remove the current line so it doesn't get in the way of the parse
    lines[current_row] = ''

    # if this is the first line of a function, for example,
    # `visitors.is_valid_import_line` will raise a failed-parse error.
    # to avoid that, lets add a `pass` line, just in-case.
    #
    indent = _find_indent(lines, current_row)
    lines.insert(current_row, indent + 'pass')

    code = '\n'.join(lines[:current_row + 1])
    return visitors.is_valid_import_line(code, row)


def next_node(code, row):
    '''<astroid.Node> or NoneType: Get the next node that is immediately after `row`.'''
    return visitors.next_node(code, row)
