#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''This module builds/maintains the priorities and rules for all settings.'''

# IMPORT STANDARD LIBRARIES
import os

# IMPORT THIRD-PARTY LIBRARIES
try:
    from cached_property import cached_property
except ImportError:
    from .vendor.cached_property import cached_property

# IMPORT LOCAL LIBRARIES
from .settings.common import SettingAbstractBaseClass
from .settings.mixin import CommonSettingMixin
from .registry import ContextMetaClass
from .registry import REGISTRY
from .core import textmate
from . import plugins


class MaximumValueExceededError(Exception):

    '''An error to describe if a value was larger than thought allowed.'''

    def __init__(self, message, errno=0, *args, **kwargs):
        '''Create the object exception.

        Args:
            message (str): The message to write for the error
            errno (:obj:`int`, optional): An optional error code,
                                          to go with the error
            *args: Passed directly to the baseclass
            *kwargs: Passed directly to the baseclass

        '''
        super(MaximumValueExceededError, self).__init__(message,
                                                        *args,
                                                        **kwargs)
        self.errno = errno


class BeginningOfLineSetting(CommonSettingMixin, SettingAbstractBaseClass):

    '''The object used to describe if snip is at the start of a line.'''

    __metaclass__ = ContextMetaClass
    _type = 'beginningOfLine'

    def __init__(self):
        '''Initialize the object instance.'''
        super(BeginningOfLineSetting, self).__init__()

    @classmethod
    def is_valid(cls, row, column, code=''):
        '''Determine if the row/column position within the code is valid.

        Args:
            row (int): The (aka line number) of the code
            column (int): The column number of the code
            code (:obj:`str`, optional): The code to check

        Returns:
            bool: If the current position is at the beginning of the line

        '''
        return column == 0


# TODO : seems to be unfinished
# TODO : Make test cases for this
#
class ClassPropertySetting(CommonSettingMixin, SettingAbstractBaseClass):

    '''An object to describe if the line is somewhere at the clsas-level.

    This context is going to be slightly tricky because, technically, although
    class properties are normally declared before a class's __init__ and its
    other methods, sometimes it's not. This class should be sensitive of those
    edge-cases.

    '''

    # TODO : combine this with classdocstring
    __metaclass__ = ContextMetaClass
    _type = 'classProperty'
    startswith = ('class ',)
    def_startswith = ('def ',)

    def __init__(self):
        '''Initialize the object instance.'''
        super(ClassPropertySetting, self).__init__()
    # end __init__

    @classmethod
    def is_in_class(cls, row, column, code=[]):
        '''Check if the given position is within the context of a class.

        Args:
            row (int): The row number of the cursor's position in the code
            column (int): The column number of the cursor
            code (:obj:`list[str]`, optional):
                The code to check the row/column of.
                If omitted, Vim's buffer is used

        Returns:
            bool: Whether or not the position lies somewhere in a class

        '''
        column -= 1
        if column < 1:
            # cannot be in a class if at the left-most margin
            return False

        for row_ in range(row, 0, -1):
            if code[row_].strip() and textmate.get_indent(code[row_]) < column:
                return code[row_].startswith(cls.startswith)
        return False
    # end is_in_class

    @classmethod
    def is_a_def_line(cls, line, allow_indent=True):
        '''Determine if the current line is a definition statement.

        TODO:
             This line is meant to just be temporary. Ideally, this should
             actually be replaced with regex.

        TODO:
            Put this function someplace else

        Args:
            line (str): The line to check if it's a definition line
            allow_indent (bool): If False, only global functions will be True

        Returns:
            bool: Whether or not the line is a definition line

        '''
        if not allow_indent and textmate.get_indent(line):
            return False

        return line.strip().startswith(cls.def_startswith)
    # end is_a_def_line

    @classmethod
    def is_valid(cls, row, column, code=''):
        '''Determine if the row/column position describes a class docstring.

        Args:
            row (int): The (aka line number) of the code
            column (int): The column number of the code
            code (:obj:`str`, optional): The code to check

        Returns:
            bool: If the current position is on a valid class definition line

        '''
        row -= 1
        code = cls.resolve_code(code=code)
        return cls.is_in_class(row=row, column=column, code=code)
# end ClassPropertySetting


def index_has_reached_maximum(index, lines):
    '''Make sure that the given row does not exceed the allowed row number(s).

    Args:
        index (int): The index value to check
        code_lines (list[str]): The lines of code to check

    Returns:
        bool: The success of the function

    '''
    try:
        lines[index]
    except IndexError:
        return True
    return False
# end index_has_reached_maximum


def get_code_context(row, column, code):
    '''Determine which context class to evaluate for the given code.

    Args:
        row (int): The row (aka line number) in the given code
        column (int): The column number in the given code
        code (str): The code (must fit within the given row/column)

    Returns:
        A context class

    '''
    code_lines = code.split('\n')
    # Fixes an issue where, if code = '', code_lines becomes []
    if not code_lines or code_lines == ['']:
        code_lines = [' ']

    if index_has_reached_maximum(index=row, lines=code_lines):
        raise MaximumValueExceededError(
            'Row: "{r}" is not allowed. Value options were "0-{l}" '
            'provided in code, "{c}"'.format(r=row, l=len(code_lines), c=code))
    elif code_lines[row] and \
            index_has_reached_maximum(index=column, lines=code_lines[row]):
        raise MaximumValueExceededError('Column: '
                                        'maximum value, "{v}" for row, "{r}"'
                                        ''.format(c=column,
                                                  v=len(code_lines[row]),
                                                  r=code_lines[row]))

    for _, class_definition in REGISTRY.iteritems():
        if class_definition.is_valid(row=row, column=column, code=code):
            return class_definition

from .plugins.class_dep import *
from .plugins.docstring import *
from .plugins.docstring_block import *
from .plugins.encoding import *
from .plugins.forloop import *
from .plugins.imports import *
from .plugins.method_dep import *
from .plugins.parenthesis import *
from .plugins.prints import *
from .plugins.shebang import *
from .plugins.text_position import *
from .plugins.tryblock import *
from .plugins.withblock import *
