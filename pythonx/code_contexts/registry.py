#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''This module's sole duty is to maintain a database of Setting objects.'''

# IMPORT STANDARD LIBRARIES
import collections


REGISTRY = collections.OrderedDict()


class ContextMetaClass(type):  # pylint: disable=R0903

    '''Registers and builds classes for every context.'''

    def __new__(mcs, clsname, bases, attrs):
        '''Automatically registers the class to the main REGISTRY.'''
        newclass = super(ContextMetaClass, mcs).__new__(
            mcs, clsname, bases, attrs)
        register_class(class_to_register=newclass)
        return newclass
    # end __new__

    def __init__(cls, *args, **kwargs):
        '''Initialize the object instance.'''
        super(ContextMetaClass, cls).__init__(*args, **kwargs)
    # end __init__
# end ContextMetaClass


def register_class(class_to_register):
    '''Add the class definition as a key/value in the REGISTRY.

    Args:
        cls_ (class): The class to add

    '''
    REGISTRY[class_to_register.__name__] = class_to_register
# end register_class


if __name__ == '__main__':
    print(__doc__)
