#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''All Setting objects related to printing, in Python.'''

# IMPORT LOCAL LIBRARIES
from ..settings.common import SettingAbstractBaseClass
from ..settings.mixin import CommonSettingMixin
from ..registry import ContextMetaClass


# TODO : Make tests for this setting
class PrintSetting(CommonSettingMixin, SettingAbstractBaseClass):

    '''A setting to make sure the current line is in a print statement.'''

    __metaclass__ = ContextMetaClass

    def __init__(self):
        '''Initialize the object instance.'''
        super(PrintSetting, self).__init__()
    # end __init__

    @classmethod
    def is_valid(cls, row, column=0, code=''):
        '''Check that the current row is not on a print statement line.

        Args:
            row (int): The (aka line number) of the code
            column (:obj:`int`, optional): The column number of the code
            code (:obj:`str`, optional): The code to check

        Returns:
            bool: Whether or not the line is a print statement line

        '''
        code = cls.resolve_code(code=code)
        return code[row].strip().startswith('print')
    # end is_valid
# end PrintSetting


if __name__ == '__main__':
    print(__doc__)
