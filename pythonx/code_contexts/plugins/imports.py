#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''This module has all import-related Setting objects within it.'''

# IMPORT STANDARD LIBRARIES
import re

# IMPORT LOCAL LIBRARIES
from ..settings.top import CommonTopSetting
from ..registry import ContextMetaClass


class ImportSetting(CommonTopSetting):

    '''A setting that describes if the user is writing an import statement.'''

    __metaclass__ = ContextMetaClass
    ignore_prefixes = ('#', '"', "'")
    _type = 'import'

    regex = \
        r'''
        (?:from[\ ]+[a-zA-Z0-9\._]+[ ]+)?
        import
            (?:
                [\ ]+(?:\*|(?:\()?[a-zA-Z0-9,\._\ ]+(?:\))?
                    (?:[\ ]+as[\ ]+[a-zA-Z0-9\._]+)?)
            )
        '''
    re_comp = re.compile(regex, re.VERBOSE)
    reload_regex = r'reload\([\w\.]+\)'
    reload_re_comp = re.compile(reload_regex)

    def __init__(self):
        '''Initialize the class object.'''
        super(ImportSetting, self).__init__()

    @classmethod
    def type(cls):
        '''str: The type of the class.'''
        return cls._type

    @classmethod
    def is_import_statement(cls, line):
        '''Check if the given line is a valid Python import statement.

        Args:
            line (str): The line to check

        Returns:
            bool: Whether or not the line is an import statement

        '''
        return bool(cls.re_comp.match(line))

    @classmethod
    def line_check(cls, line):
        '''Extend the default behavior to check if the line is an import line.

        Args:
            line (str): The line to check

        Returns:
            bool: Whether or not the line is valid

        '''
        result = super(ImportSetting, cls).line_check(line=line)
        if result:
            return result

        is_import_line = line.strip() and cls.re_comp.match(line)
        is_reload_line = line.strip() and cls.reload_re_comp.match(line)
        return is_import_line or is_reload_line


if __name__ == '__main__':
    print(__doc__)
