#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''A module the defines all Setting objects for for-loops.'''

# IMPORT LOCAL LIBRARIES
from ..settings.common import SettingAbstractBaseClass
from ..settings.mixin import CommonSettingMixin
from ..registry import ContextMetaClass
from ..core import textmate


# TODO : Needs tests for this class
class InForLoopSetting(CommonSettingMixin, SettingAbstractBaseClass):

    '''Rather than the for-loop statement, this class checks within it.'''

    __metaclass__ = ContextMetaClass
    _type = 'inForLoop'
    startswith = ('for ',)

    def __init__(self):
        '''Initialize the object instance.'''
        super(InForLoopSetting, self).__init__()
    # end __init__

    @classmethod
    def is_valid(cls, row, column=0, code=''):
        '''Check that the current row is actually within a for-loop.

        Args:
            row (int): The cursor's row position in the code
            column (:obj:`int`, optional): The column number of the code
                Note: is negligible, in this test
            code (str): The code used to check and iterate over

        Returns:
            bool: Whether or not the line was within some for-loop

        '''
        code = cls.resolve_code(code=code)
        line_indent = textmate.get_indent(code[row])

        for row_ in range(row - 1, 0, -1):
            line = code[row_]
            if not textmate.get_indent(line):
                # No more indents to check within
                break
            elif textmate.get_indent(line) < line_indent \
                    and line.strip().startswith(cls.startswith):
                return True

        return False
    # end is_valid
# end InForLoopSetting


if __name__ == '__main__':
    print(__doc__)
