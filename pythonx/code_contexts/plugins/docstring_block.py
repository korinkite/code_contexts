#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''A module that describes python "with" statement blocks.'''

# IMPORT LOCAL LIBRARIES
from ..settings.common import SettingAbstractBaseClass
from ..registry import ContextMetaClass
from .forloop import InForLoopSetting
from .docstring import DocstringSetting


# TODO : need to write tests for this
class InDocstringSphinxBlockSetting(InForLoopSetting):

    '''A generic class when code is inside blocks like Args/Returns/See Also.

    Note:
        Currently, this class steals most of its functionality from
        InForLoopSetting. If that class gets too complex, the common elements
        of both will need to be split up.

    '''

    __metaclass__ = ContextMetaClass
    _type = 'inDocstringSphinxBlock'
    startswith = ('Args:', 'Arguments:', 'Example:', 'Examples:',
                  'Keyword Args:', 'Keyword Arguments:', 'Methods', 'Note:',
                  'Notes:', 'Other Parameters:', 'Return:', 'Returns:',
                  'Raises:', 'References:', 'See Also:', 'Todo:', 'Warning:',
                  'Warnings:', 'Warns:', 'Yield:', 'Yields:')

    def __init__(self):
        '''Initialize the object instance.'''
        super(InDocstringSphinxBlockSetting, self).__init__()
    # end __init__

    @classmethod
    def is_valid(cls, row, column=0, code=''):
        '''Determine if the row/column position within a block of sphinx text.

        Note:
            If the one of the startswiths blocks are found and the
            row is inside a docstring, we know that this in a sphinx block.

        Args:
            row (int): The cursor's row position in the code
            column (:obj:`int`, optional): The column position in the code
                Note: is negligible, in this test
            code (:obj:`str`, optional): The code used to check and iterate over

        '''
        output = super(InDocstringSphinxBlockSetting, cls).is_valid(
            row=row, column=column, code=code)

        if not output:
            return False

        return DocstringSetting.is_valid(row=row, column=column, code=code)
    # end is_valid
# end InDocstringSphinxBlockSetting
