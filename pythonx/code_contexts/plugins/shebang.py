#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''A basic auto-shebang line definition.'''

# IMPORT STANDARD LIBRARIES
import os
import sys

# IMPORT LOCAL LIBRARIES
from ..settings.mixin import (CommonSettingMixin, StartingLineCommonMixin)
from ..settings.common import SettingAbstractBaseClass
from ..registry import ContextMetaClass


class ShebangSetting(CommonSettingMixin,
                     StartingLineCommonMixin,
                     SettingAbstractBaseClass):

    '''Determine if the given cursor context is on the shebang line.'''

    __metaclass__ = ContextMetaClass
    allowed_row_numbers = (0,)
    _type = 'shebang'

    def __init__(self):
        '''Initialize the class instance.'''
        super(ShebangSetting, self).__init__()
    # end __init__

    @classmethod
    def get_default(cls, interpreter=os.path.splitext(
            os.path.basename(sys.executable))[0]):
        '''str: The default shebang.'''
        return '#!{0}'.format(interpreter)
    # end get_default
# end ShebangSetting


if __name__ == '__main__':
    print(__doc__)
