#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''All Setting objects related to classes, in Python.'''

# IMPORT LOCAL LIBRARIES
from ..settings.common import SettingAbstractBaseClass
from ..settings.mixin import CommonSettingMixin
from ..registry import ContextMetaClass


# TODO : Make tests for this setting
class ClassDefSetting(CommonSettingMixin, SettingAbstractBaseClass):

    '''A setting to make sure the current line is in a class definition.'''

    __metaclass__ = ContextMetaClass
    startswith = ('class', )

    def __init__(self):
        '''Initialize the object instance.'''
        super(ClassDefSetting, self).__init__()

    @classmethod
    def is_valid(cls, row, column=0, code=''):
        '''Check that the current row is on a class definition statement line.

        Args:
            row (int): The (aka line number) of the code
            column (:obj:`int`, optional): The column number of the code
            code (:obj:`str`, optional): The code to check

        Returns:
            bool: Whether or not the line is a class defintion statement line

        '''
        code = cls.resolve_code(code=code)
        return code[row].strip().startswith(cls.startswith)


if __name__ == '__main__':
    print(__doc__)
