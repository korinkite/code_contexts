#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''A module that defines contexts based on the cursor's position in a line.'''

# IMPORT LOCAL LIBRARIES
from ..settings.common import SettingAbstractBaseClass
from ..settings.mixin import CommonSettingMixin
from ..registry import ContextMetaClass


# TODO : Needs tests for this class
class WordsPrecedingSetting(CommonSettingMixin, SettingAbstractBaseClass):

    '''This setting will check the cursor position for a word, preceding it.'''

    __metaclass__ = ContextMetaClass

    def __init__(self):
        '''Initialize the object instance.'''
        super(WordsPrecedingSetting, self).__init__()

    @classmethod
    def is_valid(cls, row, column=0, code=''):
        '''Check that the given position does not have a-z text before it.

        Args:
            row (int): The cursor's row position in the code.
            column (:obj:`int`, optional): The column number of the code
                Note: Does not do anything, in this test.
            code (str): The code used to check and iterate over.

        Returns:
            bool: Whether or not the cursor has words before it.

        '''
        code = cls.resolve_code(code=code)
        try:
            previous_char = code[row][column - 1]  # The previous character
        except IndexError:
            return False
        return previous_char.isdigit() or previous_char.isalpha()


# TODO : Needs tests for this class
class SurroundingWordsSetting(CommonSettingMixin, SettingAbstractBaseClass):

    '''Scan the word(s) around the given cursor for some information.

    If the information/surrounding words are considered valid, return True.

    '''

    __metaclass__ = ContextMetaClass

    def __init__(self):
        '''Initialize the object instance.'''
        super(SurroundingWordsSetting, self).__init__()

    @classmethod
    def is_valid(cls, row, column=0, code=''):
        '''True: Pass this function.'''
        return True

    @classmethod
    def matches_text_rule(cls,
                          snip,
                          min_count=0,
                          max_count=9001,
                          direction='left',
                          excluded_chars=None,
                          excluded_words=None):
        '''Check if the given snippet's cursor matches the text rules given.

        This Setting is perfect for scenarios when you only want a context to
        pass if certain word(s) come before or after the given snippet's
        cursor location.

        Args:
            min_count (:obj:`bool`, optional): The minimum amount of words
                                               required to pass. Default: 0.
            max_count (:obj:`int`, optional):
                The most number of words allowed to process. If the word count
                exceeds this number, the method returns False.
                Default: some very high number (9001).
            direction (:obj:`str`, optional): The direction to look in.
                                              Options: 'left', 'right'.
            excluded_chars (:obj:`list[str]`, optional):
                If any character is included and one or more of the words
                processed contains any of these characters, return False.
                Default: [].
            excluded_words (:obj:`list[str]`), optional):
                If any word in this list appears while the words in this
                code's row are being processed, return False.
                Default: [].

        '''
        row = snip.line
        column = snip.column
        code = snip.buffer

        # Just some convenience keys to reasonably try and catch user input
        directions = \
            {
                'left': 'left',
                'right': 'right',
                'before': 'left',
                'after': 'right',
            }
        try:
            direction = directions[direction.lower()]
        except KeyError:
            raise ValueError('Direction: "{dir_}" was invalid. Options were, '
                             '"{opt}".'.format(dir_=direction,
                                               opt=sorted(directions.keys())))

        if excluded_chars is None:
            excluded_chars = []

        if excluded_words is None:
            excluded_words = []

        code = cls.resolve_code(code=code)

        try:
            current_row = code[row]
        except KeyError:
            return False

        if direction == 'left':
            process_row = current_row[:column].lstrip()
        elif direction == 'right':
            process_row = current_row[column:].lstrip()

        words = [word for word in process_row.split(' ') if word]
        if len(words) < min_count or len(words) > max_count:
            return False

        for word in words:
            if word in excluded_words:
                return False

            for char in excluded_chars:
                if char in word:
                    return False
        return True


if __name__ == '__main__':
    print(__doc__)
