#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''A module that describes python "with" statement blocks.'''

# IMPORT LOCAL LIBRARIES
from ..settings.common import SettingAbstractBaseClass
from ..settings.mixin import CommonSettingMixin
from ..registry import ContextMetaClass


class InWithSetting(CommonSettingMixin, SettingAbstractBaseClass):

    '''The object used to describe if snip is inside of a "with" block.'''

    __metaclass__ = ContextMetaClass
    startswith = ('with',)
    _type = 'with'

    def __init__(self):
        '''Initialize the object instance.'''
        super(InWithSetting, self).__init__()
    # end __init__

    @classmethod
    def is_valid(cls, row, column, code='', in_line=True):
        '''Determine if the row/column position within the code is valid.

        Args:
            row (int): The (aka line number) of the code
            column (int): The column number of the code
            code (:obj:`str`, optional): The code to check
            in_line (:obj:`bool`, optional):
                If True, this function only returns True if the code row is
                on the same line as the with definition. If False, the code
                can be anywhere within the with's indented block.

        Returns:
            bool: If the current position is on/in a with block

        '''
        if not in_line:
            raise NotImplementedError('Havent built this part of the function')
        code = cls.resolve_code(code=code)

        return in_line and code[row].strip().startswith(cls.startswith)
    # end is_valid
# end InWithSetting
