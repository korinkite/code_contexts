#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''A module for all generic docstring Setting objects.

This module has a likelihood to grow and gain complexity.
If this happens, the classes in this module should be moved to another
location.

'''

# IMPORT STANDARD LIBRARIES
import os
import sys

# IMPORT THIRD-PARTY LIBRARIES
import six
import vim

# IMPORT LOCAL LIBRARIES
from ..settings.common import SettingAbstractBaseClass
from ..settings.mixin import CommonSettingMixin
from ..settings.top import CommonTopSetting
from ..registry import ContextMetaClass


class DocstringSetting(CommonSettingMixin, SettingAbstractBaseClass):

    '''The object will describe when the code is located in a docstring.'''

    # TODO : Figure out if it's really necessary to include pythonSpaceError
    __metaclass__ = ContextMetaClass
    expected_syn_output = ('pythonDocstring', 'pythonString', 'pythonComment',
                           'Constant', 'String')
    _type = 'docstring'

    def __init__(self):
        '''Initialize the class object.'''
        super(DocstringSetting, self).__init__()
    # end __init__

    @classmethod
    def is_docstring_line(cls, row, column, code):
        '''Check if the given row/column is valid docstring line.

        Args:
            row (int): The (aka line number) of the code
            column (int): The column number of the code
            code (str): The code to check

        '''
        def get_syntax_group_name(column):
            output = vim.command(
                'let g:some_var = synIDattr(synID(line("."), {num}, 0), '
                '"name")'.format(num=column))
            return vim.eval('g:some_var')

        # TODO : This function is using "." and doesn't actually use row/col!!!
        current_column = int(vim.eval('col(".")'))

        for index in reversed(six.moves.range(current_column)):
            output = get_syntax_group_name(column - 1)
            if output and output in cls.expected_syn_output:
                return True
        return False

    @classmethod
    def is_valid(cls, row, column, code=''):
        '''Determine if the row/column position within the code is valid.

        Args:
            row (int): The (aka line number) of the code
            column (int): The column number of the code
            code (:obj:`str`, optional): The code to check

        Returns:
            bool: If the current position is at the shebang line

        '''
        code = cls.resolve_code(code=code)
        return cls.is_docstring_line(row=row, column=column, code=code)
    # end is_valid
# end DocstringSetting


class ModuleDocstringSetting(CommonTopSetting):

    '''Determine if the given position is able to be a module docstring.'''

    __metaclass__ = ContextMetaClass
    interpreter = os.path.splitext(os.path.basename(sys.executable))[0]
    ignore_prefixes = ('#',)
    _type = 'docstring'

    def __init__(self):
        '''Initialize the class instance.'''
        super(ModuleDocstringSetting, self).__init__()
    # end __init__

    @classmethod
    def type(cls):
        '''str: The type of the class.'''
        return cls._type
    # end type
# end ModuleDocstringSetting


class ClassDocstringSetting(CommonSettingMixin, SettingAbstractBaseClass):

    '''The object used to describe if snip is directly below a class.'''

    __metaclass__ = ContextMetaClass
    ignore_prefixes = ('', '#')
    _type = 'classDocstring'
    startswith = ('class ',)
    #  regex = 'class[ ]+[a-zA-Z0-9_]+(?:[ ]+)?(\((?:[a-zA-Z0-9_, ]+)?\))?:'
    #  re_comp = re.compile(regex)

    def __init__(self):
        '''Initialize the object instance.'''
        super(ClassDocstringSetting, self).__init__()
    # end __init__

    @classmethod
    def is_valid(cls, row, column, code=''):
        '''Determine if the row/column position describes a class docstring.

        Args:
            row (int): The (aka line number) of the code
            column (int): The column number of the code
            code (:obj:`str`, optional): The code to check

        Returns:
            bool: If the current position is on a valid class definition line

        '''
        row -= 1
        code = cls.resolve_code(code=code)

        for row_ in range(row, 0, -1):
            if code[row_].strip().startswith(cls.startswith):
                return True

            if code[row_].strip() not in cls.ignore_prefixes:
                return False

        return False
    # end is_valid
# end ClassDocstringSetting


class FunctionDocstringSetting(ClassDocstringSetting):

    '''A subclass for testing for methods/function definition lines.

    Since class/function docstrings are similar, it's safe to inherit directly.

    '''

    startswith = ('def ',)

    def __init__(self):
        '''Initialize the object instance.'''
        super(FunctionDocstringSetting, self).__init__()
    # end __init__
# end FunctionDocstringSetting


if __name__ == '__main__':
    print(__doc__)
