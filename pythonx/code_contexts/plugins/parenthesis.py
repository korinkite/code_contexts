#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''A module that helps write Setting objects that exist with parenthesis.'''

# IMPORT STANDARD LIBRARIES
import re

# IMPORT LOCAL LIBRARIES
from ..settings.common import SettingAbstractBaseClass
from ..settings.mixin import CommonSettingMixin
from ..registry import ContextMetaClass


# TODO : A WIP class
class InParenthesisSetting(CommonSettingMixin, SettingAbstractBaseClass):

    '''A setting to determine if the cursor position is with a pair of ()s.'''

    __metaclass__ = ContextMetaClass
    open_str = '('
    close_str = ')'

    def __init__(self, nested=False, requires_open=True, requires_close=False):
        '''Initialize the object instance.'''
        super(InParenthesisSetting, self).__init__()
    # end __init__

    @classmethod
    def is_valid(cls, row, column, code=''):
        code = cls.resolve_code(code=code)
        current_row = code[row]
        has_opening_paren = False
        has_closing_paren = False
        for character in current_row[column:]:
            if character == cls.close_str:
                has_closing_paren = True
                break

        for character in current_row[:column]:
            if character == cls.open_str:
                has_opening_paren = True
                break

        return has_closing_paren and has_opening_paren


class ComphrensionSetting(CommonSettingMixin, SettingAbstractBaseClass):

    '''The object to describe if snip is inside of a list/generator comp.'''

    __metaclass__ = ContextMetaClass
    _type = 'comprehension'
    regex = \
        r'''
        (?P<first_brace>\[|\()(?:.+)?
        (?P<second_brace>\]|\))?
        '''
    re_comp = re.compile(regex, re.VERBOSE)

    def __init__(self):
        '''Initialize the class object.'''
        super(ComphrensionSetting, self).__init__()
    # end __init__

    @classmethod
    def is_valid(cls, row, column, code=''):
        '''Determine if the row/column position within the code is valid.

        Args:
            row (int): The (aka line number) of the code
            column (int): The column number of the code
            code (:obj:`str`, optional): The code to check

        Returns:
            bool: If the current position is at the shebang line

        '''
        code = cls.resolve_code(code=code)
        for match in cls.re_comp.finditer(code[row]):
            if column >= match.start() and column < match.end():
                return True
        return False
    # end is_valid
# end ComphrensionSetting


if __name__ == '__main__':
    print(__doc__)
