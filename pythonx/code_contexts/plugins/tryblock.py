#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''A module that describes python "with" statement blocks.'''

# IMPORT LOCAL LIBRARIES
from ..settings.common import SettingAbstractBaseClass
from ..settings.mixin import CommonSettingMixin
from ..registry import ContextMetaClass
from ..core import textmate


class TryDescriptionSetting(CommonSettingMixin, SettingAbstractBaseClass):

    '''The object will describe if code in or directly after try/except.

    This class is a bit special becaue it will yield True if the code is
    within the try block, as well as if it's on the same line (for example,
    if the user is about to write "except"/"else"/etc)

    '''

    __metaclass__ = ContextMetaClass
    startswith = ('try:',)
    _type = 'try'

    def __init__(self):
        '''Initialize the class object.'''
        super(TryDescriptionSetting, self).__init__()
    # end __init__

    @classmethod
    def is_valid(cls, row, column, code='', in_line=False):
        '''Determine if the row/column position within the code is valid.

        Args:
            row (int): The (aka line number) of the code
            column (int): The column number of the code
            code (:obj:`str`, optional): The code to check
            in_line (:obj:`bool`, optional):
                If True, this function only returns True if the code row is
                on the same line as the with definition. If False, the code
                can be anywhere within the with's indented block.

        Returns:
            bool: If the current position is at the shebang line

        '''
        code = cls.resolve_code(code=code)

        cur_line = code[row]
        if in_line:
            return cur_line.strip().startswith(cls.startswith)

        cur_line_indent = textmate.get_indent(cur_line)

        for line_lookup in range(row - 1, 0, -1):
            previous_line = code[line_lookup].strip()
            previous_line_indent = textmate.get_indent(code[line_lookup])
            if not previous_line or \
                    (previous_line and previous_line_indent > cur_line_indent):
                continue
            elif previous_line_indent < cur_line_indent:
                return False

            return previous_line.startswith(cls.startswith)
    # end is_valid
# end TryDescriptionSetting


if __name__ == '__main__':
    print(__doc__)
