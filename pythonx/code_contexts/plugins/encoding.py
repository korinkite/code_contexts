#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''A module that writes the encoding line, below the shebang line.'''

# IMPORT LOCAL LIBRARIES
from ..settings.mixin import (CommonSettingMixin, StartingLineCommonMixin)
from ..settings.common import SettingAbstractBaseClass
from ..registry import ContextMetaClass


class EncodingSetting(CommonSettingMixin,
                      StartingLineCommonMixin,
                      SettingAbstractBaseClass):

    '''Determine if the given cursor context is on the encode-line.'''

    __metaclass__ = ContextMetaClass
    allowed_row_numbers = (1,)
    _type = 'encoding'

    def __init__(self):
        '''Initialize the class instance.'''
        super(EncodingSetting, self).__init__()
    # end __init__

    @classmethod
    def get_default(cls, encoding='utf-8'):
        '''str: The default encoding for a Python file.'''
        return '# -*- coding: {c} -*-'.format(c=encoding)
    # end get_default
# end EncodingSetting
