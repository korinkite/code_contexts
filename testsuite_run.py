#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Runs all of the test cases found in the current root package.'''

# IMPORT STANDARD LIBRARIES
import unittest

# IMPORT THIRD-PARTY LIBRARIES
import vimmock


def main():
    '''The beginning of the testsuite's discovery/execution.'''
    vimmock.patch_vim()
    suite = unittest.TestLoader().discover('.')
    unittest.TextTestRunner(verbosity=2).run(suite)
# end main


if __name__ == '__main__':
    main()
