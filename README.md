This repository requires cached_property 1.3.1+. If cached_property isn't
installed and on the user's PYTHONPATH, a locally-copied cached_property will
be imported and used, instead

Install cached_property with this command:

```
pip install cached_property
```
